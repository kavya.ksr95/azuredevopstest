﻿//using CSharpAssignment_Oct17;
//using CSharpAssignment_Oct17.Helpers;
//using CSharpAssignment_Oct17.Pages;
//using NUnit.Framework;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace CSharpAssignment_OCT17.TestCases
//{
//    public class ParallelizableTest
//    {
//        [TestFixture]
//        [Parallelizable]
//        public class Assignement : TestBase
//        {
//            HomePage home => new HomePage(driver, extentTest);
//            Assertion assertion;

//            [SetUp]
//            public void Initilize()
//            {
//                assertion = new Assertion(extentTest);
//            }

//            [Test]
//            public void ParallelExecutionTest()
//            {
//                var build = home.HoverAndClickingBuildAutomationLabel();
//                build.BuildAutomationENewsletter.Click();
//                int newsTitleCount = build.GetNewsTitles();
//                assertion.IsTrue(newsTitleCount > 0, "Validating News title count");
//            }
//        }
//    }
//}