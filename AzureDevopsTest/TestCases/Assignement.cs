using CSharpAssignment_Oct17.Helpers;
using CSharpAssignment_Oct17.Pages;
using NUnit.Framework;

namespace CSharpAssignment_Oct17.Tests
{
    [TestFixture]
    [Parallelizable]
    public class Assignement : TestBase
    {
        HomePage home => new HomePage(driver, extentTest);
        Assertion assertion;

        [SetUp]
        public void Initilize()
        {
            assertion = new Assertion(extentTest);
        }
        
        [Test]
        public void AssignmentOneExecution()
        {
            var build = home.HoverAndClickingBuildAutomationLabel();
            build.BuildAutomationENewsletter.Click();
            int newsTitleCount = build.GetNewsTitles();
            assertion.IsTrue(newsTitleCount > 0, "Validating News title count");
        }

        
        //[Test]
        //public void AssignmentTwoExecution()
        //{
        //    home.ProductsLable.Click();
        //    ProductsPage product = new ProductsPage(driver, extentTest);
        //    product.GetTabSequenceTitle(product.ProductSearchTitle);
        //    assertion.AreEqual(product.finalText, "Product Search - Automation, Control & Instrumentation Products","Comparing Title");
        //}


        //[Test]
        //public void AssignmentThreeExecution()
        //{
        //    var salary = home.HoverAndClickingSalarySurveyListPage();
        //    salary.getAvgSalaryByRegion("South Asia");
        //    salary.getAvgSalaryRegionOfUnitedStates("West South Central (South)");
        //}
    }

}
