﻿using AventStack.ExtentReports;
using NUnit.Framework;
using System;


namespace CSharpAssignment_Oct17.Helpers
{
    public class Assertion
    {
        public ExtentTest extentTest;

        public Assertion(ExtentTest _extentTest)
        {
            extentTest = _extentTest;
        }
        /// <summary>
        /// Assert IsTrue
        /// </summary>
        /// <param name="isExpectedValue"></param>
        /// <param name="description"></param>
        public void IsTrue(bool isExpectedValue, string description)
        {
            try
            {
                Assert.IsTrue(isExpectedValue, description);
                extentTest.Log(Status.Pass, description + " Assert IsTrue is Passing." + "<br />Expected value: " + isExpectedValue);
            }
            catch (Exception e)
            {
                extentTest.Log(Status.Fail, description + "  Assert IsTrue is Failing." + "<br />Expected value: " + isExpectedValue);
                throw e;
            }
        }



        /// <summary>
        /// Assert IsFalse
        /// </summary>
        /// <param name="isExpectedValue"></param>
        /// <param name="description"></param>
        public void IsFalse(bool isExpectedValue, string description)
        {
            try
            {
                Assert.IsFalse(isExpectedValue, description);
                extentTest.Log(Status.Pass, description + "  Assert IsFalse is Passing." + "<br />Expected value: " + isExpectedValue);
            }
            catch (Exception e)
            {
                extentTest.Log(Status.Fail, description + "  Assert IsFalse is Failing." + "<br />Expected value: " + isExpectedValue);
                throw e;
            }
        }



        /// <summary>
        /// Assert areEqual for bool type
        /// </summary>
        /// <param name="isExpectedValue"></param>
        /// <param name="isActualValue"></param>
        /// <param name="description"></param>
        public void AreEqual(bool isExpectedValue, bool isActualValue, string description)
        {
            try
            {
                Assert.AreEqual(isExpectedValue, isActualValue, description);
                extentTest.Log(Status.Pass, description + "  Assert AreEqual is Passing." + "<br />Expected value: " + isExpectedValue + " and Actual Value: " + isActualValue);
            }
            catch (Exception e)
            {
                extentTest.Log(Status.Fail, description + "  Assert AreEqual is Failing." + "<br />Expected value: " + isExpectedValue + " and Actual Value: " + isActualValue);
                throw new Exception("Assert AreEqual failed " + e.Message);
            }
        }
        /// <summary>
        /// Assert areEqual for int type
        /// </summary>
        /// <param name="isExpectedValue"></param>
        /// <param name="isActualValue"></param>
        /// <param name="description"></param>
        public void AreEqual(int isExpectedValue, int isActualValue, string description)
        {
            try
            {
                Assert.AreEqual(isExpectedValue, isActualValue, description);
                extentTest.Log(Status.Pass, description + "  Assert AreEqual is Passing." + "<br />Expected value: " + isExpectedValue + " and Actual Value: " + isActualValue);
            }
            catch (Exception e)
            {
                extentTest.Log(Status.Fail, description + "  Assert AreEqual is Failing." + "<br /> Expected value: " + isExpectedValue + " and Actual Value: " + isActualValue);
                throw new Exception("Assert AreEqual failed " + e.Message);
            }
        }



        /// <summary>
        /// Assert AreEqual for string type
        /// </summary>
        /// <param name="isExpectedValue"></param>
        /// <param name="isActualValue"></param>
        /// <param name="description"></param>
        public void AreEqual(string isExpectedValue, string isActualValue, string description)
        {
            try
            {
                Assert.AreEqual(isExpectedValue, isActualValue, description);
                extentTest.Log(Status.Pass, description + "  Assert AreEqual is Passing." + "<br />Expected value: " + isExpectedValue + " and Actual Value: " + isActualValue);
            }
            catch (Exception e)
            {
                extentTest.Log(Status.Fail, description + "  Assert AreEqual is Failing." + "<br />Expected value: " + isExpectedValue + " and Actual Value: " + isActualValue);
                throw new Exception("Assert AreEqual failed " + e.Message);
            }
        }
    }
}
