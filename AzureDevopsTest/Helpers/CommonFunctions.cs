using AventStack.ExtentReports;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Configuration;
using System.Diagnostics;

namespace CSharpAssignment_Oct17.Helpers
{
    public class CommonFunctions
    {
        private IWebDriver driver;
        private ExtentTest extentTest;

        #region Common methods
       
        /// <summary>
        /// wait for element to be clickable
        /// </summary>
        /// <param name="element">element</param>
        public void WaitForElementToBeClickable(By elementBy, string filedName)
        {
            try
            {
                IWebElement element = driver.FindElement(elementBy);
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(Convert.ToInt64(ConfigurationManager.AppSettings["Timeout"])));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(elementBy));

                extentTest.Log(Status.Pass, "wait for element clickable: " + filedName);
            }
            catch (Exception e)
            {
                extentTest.Log(Status.Fail, "wait for element clickable failed for field: " + filedName);
                throw e;

            }
        }
        /// <summary>
        /// wait for element to be visible
        /// </summary>
        /// <param name="element">element</param>
        public void WaitForElementIsVisible(By elementBy, string filedName)
        {
            try
            {
                var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 20));
                var element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(elementBy));
                extentTest.Log(Status.Pass, "wait for element visible" + filedName);
            }
            catch (Exception e)
            {
                extentTest.Log(Status.Fail, "wait for element visible failed for field: " + filedName);
                throw e;

            }
        }

        public static void KillAllDriverProcesses()
        {
            Process[] chromeDriverProcesses = Process.GetProcessesByName("chromedriver");
            foreach (var chromeDriverProcess in chromeDriverProcesses)
            {
                chromeDriverProcess.Kill();
            }
        }

        #endregion

    }
}
