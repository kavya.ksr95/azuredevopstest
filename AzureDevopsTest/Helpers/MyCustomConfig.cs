﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_OCT17.Helpers
{
    public class MyCustomConfig
    {
        public const string Browser = "browser";

        public const string Url = "url";

        public const string Timeout = "timeout";

        /// <summary>
        /// Method to read and fetch the key value from app.config file
        /// </summary>
        /// <param name="key"></param>
        public static string GetConfigKey(string key)
        {
            return ConfigurationManager.AppSettings[key] ?? throw new Exception("Invalid Key" + key);
        }
        /// <summary>
        /// Adding and updating value into App.config file
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void AddUpdateKeyAndValue(string key, string value)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            if (settings[key] == null)
            {
                settings.Add(key, value);
            }
            else
            {
                settings[key].Value = value;
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }


        /// <summary>
        /// Deleting value from App.config
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveKeyAndValue(string key)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            if (settings[key] != null)
            {
                settings.Remove(key);
            }

            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);




        }

    }
   
}
