﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Helpers
{
   public class DriversFactory
    {
        public IWebDriver GetDriver(string browserName)
        {
            switch (browserName.ToUpper())
            {
                case "CHROME":
                    return GetChromeDriver();
                case "FIREFOX":
                    return GetFireFoxDriver();
                default:
                    return GetChromeDriver();
            }
        }

        private IWebDriver GetChromeDriver()
        {
            string chromeDriverPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory);
            ChromeOptions options = new ChromeOptions();
            //Disabling the infobar which warns any dangerous information
            //disable - infobar doesn't work anymore.
            options.AddArguments("--start-maximized");
            options.AddExcludedArgument("enable-automation");
            options.AddAdditionalCapability("useAutomationExtension", false);
            IWebDriver driver = new ChromeDriver(chromeDriverPath, options);
            driver.Manage().Window.Maximize();
            return driver;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IWebDriver GetFireFoxDriver()
        {
            string firfoxDriverPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Drivers");
            IWebDriver driver = new FirefoxDriver(firfoxDriverPath);
            driver.Manage().Window.Maximize();
            return driver;
        }
    }
}

