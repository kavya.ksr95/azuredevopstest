﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Helpers
{
    public class FileHelper
    {
        public static string GenerateRandomFileName()
        {
            return "TestReport_" + DateTime.Now.ToString("yyyy-MM-dd_HH-MM-ss") + ".html";
        }
    }
}
