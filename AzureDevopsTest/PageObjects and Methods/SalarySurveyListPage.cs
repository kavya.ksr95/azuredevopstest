using AventStack.ExtentReports;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Threading;

namespace CSharpAssignment_Oct17.Pages
{
    public class SalarySurveyListPage : PageBase
    {
        public SalarySurveyListPage(IWebDriver driver, ExtentTest test) : base(driver, test)
        {
            
        }

        #region elements
     
        public IWebElement mouseHoverOnJobCenter => driver.FindElement(By.XPath("//*[@id='nav']/li[4]/a"));

        public IWebElement clickOn2018SalarySurveyResult => driver.FindElement(By.XPath("//*[@id='nav']/li[4]/div/div/ul/li[3]/a"));

        #endregion

        #region Method
        //Method to get the avg salary of respective regions 
        public void getAvgSalaryByRegion(string RegionOfTheWorld)
        {
            Actions action = new Actions(driver);
            action.MoveToElement(mouseHoverOnJobCenter).Perform();
            Thread.Sleep(4000);
            clickOn2018SalarySurveyResult.Click();
            IWebElement AverageSalary = driver.FindElement(By.XPath("//table[2]/tbody/tr/td//*[contains(text(), '" + RegionOfTheWorld + "')]/ancestor::td/following-sibling::td[1]"));
            Console.WriteLine("Average Salary of Region of the world South Asia is =" + AverageSalary.Text);
        }

        public void getAvgSalaryRegionOfUnitedStates(string RegionOfTheUnitedStates)
        {
            IWebElement PercentRespondents = driver.FindElement(By.XPath("//table[3]/tbody/tr/td//*[contains(text(),'" + RegionOfTheUnitedStates + "')]/ancestor::td/following-sibling::td[2]"));
            Console.WriteLine("Percent Respondents of Region of the United States West south central is =" + PercentRespondents.Text);
        }
    }

        #endregion
    }

