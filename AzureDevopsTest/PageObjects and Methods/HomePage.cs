using AventStack.ExtentReports;
using CSharpAssignment_Oct17.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpAssignment_Oct17.Pages
{
    public class HomePage :PageBase
    {
        public HomePage(IWebDriver driver,ExtentTest test) :base(driver,test)
        {
            headerTab.Click();
            WaitForElementToBeVisible(By.CssSelector("li.newnav.right:nth-child(4)"));
        }

    #region Elements
        private IWebElement IndustryLabel => driver.FindElement(By.XPath("//*[@id='menu']/li/div/a[contains(@href,'industries')]/span[text()='Industries']"));
        private IWebElement BuildAutomationLabel => driver.FindElement(By.XPath("//*[@id='menu']/li[contains(@class,'last-child')]/div/div/ul/li/a[contains(@href,'building-automation')]"));
        public IWebElement ProductsLable => driver.FindElement(By.CssSelector(".newnav a[href^='/product']"));
        private IWebElement JobCenter => driver.FindElement(By.CssSelector("li.newnav.right:nth-child(4)"));
        private IWebElement salarySurveyResult2018 => driver.FindElement(By.XPath("//div[@id='header']//li[4]//div[1]//div[1]//li[3]"));

        private IWebElement headerTab => driver.FindElement(By.ClassName("header-box-new"));
        //dummy comment

        #endregion

        #region Methods
        public BuildingAutomationPage HoverAndClickingBuildAutomationLabel()
        {
            Actions actions = new Actions(driver);
            actions.MoveToElement(IndustryLabel).Build().Perform();
            BuildAutomationLabel.Click();
            return new BuildingAutomationPage(driver, extentTest);
        }
     
       
        public void GetTitle(IWebElement webElement)
        {
            webElement.GetAttribute("textContent");
        }
        public SalarySurveyListPage HoverAndClickingSalarySurveyListPage()
        {
            Actions actions = new Actions(driver);
            actions.MoveToElement(JobCenter).Build().Perform();
            salarySurveyResult2018.Click();
            return new SalarySurveyListPage(driver, extentTest);
        }
        #endregion
    }
}
