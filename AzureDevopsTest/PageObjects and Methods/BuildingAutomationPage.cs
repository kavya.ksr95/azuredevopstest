using AventStack.ExtentReports;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace CSharpAssignment_Oct17.Pages
{
    public class BuildingAutomationPage : PageBase
    {
        public BuildingAutomationPage(IWebDriver driver, ExtentTest test) : base(driver, test)
        {

        }
        #region Elements

        public IWebElement BuildAutomationENewsletter => driver.FindElement(By.XPath("//div[@class='holder']/ul[@class='sub-nav']/li/a[contains(@href,'e-newsletter')]"));
        IReadOnlyCollection<IWebElement> listLetters => driver.FindElements(By.XPath("//div[@class='company-section add']//ul/li"));

        #endregion

        #region Methods

        public int GetNewsTitles()
        {
            Console.WriteLine("Total news = " + listLetters.Count);

            foreach (var letters in listLetters)
            {
                Console.WriteLine(letters.Text);
            }
            if (listLetters.Count > 0)
            {
                return listLetters.Count;
            }
            else
            {
                throw new Exception("Failed to retrive News titles");
            }
        }

    }
        
           
        #endregion
 }

