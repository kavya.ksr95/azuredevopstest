using AventStack.ExtentReports;
using CSharpAssignment_Oct17.Helpers;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CSharpAssignment_Oct17.Pages
{
    public class ProductsPage : PageBase
    {
        Assertion assertion;
        public ProductsPage(IWebDriver driver, ExtentTest test) : base(driver, test)
        {
            assertion = new Assertion(extentTest);
            WaitForElementToBeVisible(By.XPath("//div[@class='block']/h1"));
        }
        public string finalText;
        #region Elements

        public IWebElement ProductSearchTitle => driver.FindElement(By.XPath("//div[@class='block']/h1"));

        #endregion

        #region Methods


        public void GetTabSequenceTitle(IWebElement webElement)
        {
           finalText = webElement.GetAttribute("textContent");

        }

        public void VerifyTitle(string finalText,string expectedTitle)
        {
            assertion.AreEqual(finalText,expectedTitle,"Comparing title");
        }
        #endregion
    }
}
