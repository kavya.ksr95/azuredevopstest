using AventStack.ExtentReports;
using CSharpAssignment_OCT17.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Configuration;

namespace CSharpAssignment_Oct17
{
    public class PageBase
    {
        protected IWebDriver driver;
        protected ExtentTest extentTest;
        private int waitTime;
         
        public PageBase(IWebDriver driver, ExtentTest test)
        {
            this.driver = driver;
            this.extentTest = test;
            waitTime = int.Parse(MyCustomConfig.GetConfigKey(MyCustomConfig.Timeout));
        }
        /// <summary>
        ///     Wait for element to ve visible
        /// </summary>
        /// <param name="element"></param>
        /// <param name="seconds"></param>
        /// <returns></returns>
        protected IWebElement WaitForElementToBeVisible(By locator,int seconds = -1)
        {
            if(seconds == -1)
                seconds = waitTime;
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(seconds));
            return wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(locator));
        }
    }
}
