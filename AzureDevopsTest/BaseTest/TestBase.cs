
using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using CSharpAssignment_Oct17.Helpers;
using CSharpAssignment_OCT17.Helpers;
using NUnit.Framework;
using NUnit.Framework.Interfaces;
using OpenQA.Selenium;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;

namespace CSharpAssignment_Oct17
{
    public abstract class TestBase
    {
        public IWebDriver driver;
        public static ExtentReports extent;
        public static ExtentTest extentTest;
        string screenshotFolder = "";
        string projectPath = "";
        string reportFullPath = "";
        ExtentHtmlReporter htmlReporter;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            
            /*Get current path to project*/
            string path = System.Reflection.Assembly.GetCallingAssembly().CodeBase;
            string actualPath = path.Substring(0, path.LastIndexOf("bin"));


            /*Get current path to project before debug folder */
            projectPath = new Uri(actualPath).LocalPath;
            reportFullPath = Path.Combine(projectPath+"Reports\\AutomationReport"+ DateTime.Now.ToString(" MM/dd/yyyy HH:mm:ss").Replace(" ", "__"));

            extent = new ExtentReports();
            /* Set the reporter object */
            htmlReporter = new ExtentHtmlReporter(reportFullPath+".html"); 
            extent.AttachReporter(htmlReporter);
        }

        [SetUp]
        public void SetUp()
        {
            // starting test

            extentTest = extent.CreateTest(TestContext.CurrentContext.Test.Name);

            //Launching the browser
            DriversFactory driverFactory = new DriversFactory();

           string browser = MyCustomConfig.GetConfigKey(MyCustomConfig.Browser);
            //Navigating to the UR
            driver = driverFactory.GetDriver(browser);

            //Navigating to the website
            driver.Navigate().GoToUrl(MyCustomConfig.GetConfigKey(MyCustomConfig.Url));

            //Current test case name

            extentTest.Log(Status.Info, "Executing Test case :" + TestContext.CurrentContext.Test.Name);

        }


        [TearDown]
        public void TearDown()
        {
            //Switch case to log status of each test
            var status = TestContext.CurrentContext.Result.Outcome.Status;
            var stackTrace = "" + TestContext.CurrentContext.Result.StackTrace + "";
            var errorMessage = TestContext.CurrentContext.Result.Message;
            switch (status)
            {
                case TestStatus.Skipped:
                    extentTest.Log(Status.Skip, stackTrace + errorMessage);
                    break;
                case TestStatus.Passed:
                    extentTest.Log(Status.Pass, stackTrace + errorMessage);
                    break;
                case TestStatus.Warning:
                    extentTest.Log(Status.Warning, stackTrace + errorMessage);
                    break;
                case TestStatus.Failed:
                    extentTest.Log(Status.Fail, stackTrace + errorMessage);
                    extentTest.Log(Status.Fail, MediaEntityBuilder.CreateScreenCaptureFromPath(CaptureScreenShot(driver, TestContext.CurrentContext.Test.Name + DateTime.Now.ToString("MM.dd.yyyy hh.mm.ss").Replace(" ", "_"))).Build());
                    break;
                default:
                    break;
            }
            //End test report
            //extent.RemoveTest(extentTest);


        }

        /// <summary>
        /// Capture screenshot
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="screenShotName"></param>
        /// <returns></returns>
        public string CaptureScreenShot(IWebDriver driver, string screenShotName)
        {
            string localpath = string.Empty;
            try
            {
                screenshotFolder = reportFullPath + "\\Screenshots";
                if (!System.IO.Directory.Exists(screenshotFolder))
                {
                    Directory.CreateDirectory(screenshotFolder);

                }
                ITakesScreenshot ts = (ITakesScreenshot)driver;
                Screenshot screenshot = ts.GetScreenshot();
                string finalpth = screenshotFolder + "\\" + screenShotName + ".png";
                localpath = new Uri(finalpth).LocalPath;
                screenshot.SaveAsFile(localpath, ScreenshotImageFormat.Png);
                return localpath;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            
            extent.Flush();
            driver.Quit();
            CommonFunctions.KillAllDriverProcesses();


        }
    }
}
 